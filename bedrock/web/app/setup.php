<?php 

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('chiesi/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('chiesi/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
}, 100);